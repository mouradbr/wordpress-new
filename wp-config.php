<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'sushiwan' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'sushiwan.digit-dev.com' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'T=oeT>dm9rv[LMp~0 1]FW%|}xu@ofnWg,9?2qwO4X`dBEedB-q|5{E,$JN=zZj/' );
define( 'SECURE_AUTH_KEY',  'G%d?&icTz}2%EE_h+;GImsx@m)T81a,&Zv<M_>`PT~8fnmBW6kA[~ZE*#RA-[}/d' );
define( 'LOGGED_IN_KEY',    '$kIk?r%$rr3YS~,[iP<H+W6C3J4`T+`ab=:Di{8B&PJJ9%AwlQoR1.{u#7SXXvhv' );
define( 'NONCE_KEY',        '00:R>!8?p3*xV!{`0`4kOOD(IPStgSea@fg,?5?a5N,%cnZ0yrAol3n|OLI33e^@' );
define( 'AUTH_SALT',        'teY^j/a8rdcZjuBRA/#:sM>DU5KP;-%<nv 8HzVk=I&UlpIblu+TF[ba=Kn4AHwT' );
define( 'SECURE_AUTH_SALT', 'Zqk9o@:K+~@6,~nM,yjQ_c<9 Jj>S$&Jxda<-t7E)cyb-_njfedhOy]4Xd]Lf=`}' );
define( 'LOGGED_IN_SALT',   '?VN>IXkJDi0LmK#R_5<.DP@tf:8]Sk`9O]tbM/oaVP<X?Hp+KFurL{1qI=+e3?dn' );
define( 'NONCE_SALT',       'Zw2M}1QL9rxmu#`;*VYd%y,g1Cu)kXpi>Y-hc@`5w1lF>NN3Z95>~1k0x@>uKcU-' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
